From openjdk:8-jdk-alpine
WORKDIR=abcd
EXPOSE 8080 443 80 22 8761
COPY /target/eureka-server-0.0.1-SNAPSHOT.jar eureka-server-0.0.1-SNAPSHOT.jar
CMD ["java","-jar","eureka-server-0.0.1-SNAPSHOT.jar"]
